// Copyright 2007 Fabien Cazenave, all rights reserved.

var gLecture = null;    // xml document
var gLessons = Array(); // xml nodes

function loadLessons(language) {
  gLecture = document.implementation.createDocument("", "", null);
  gLecture.onload = initLessons;
  gLecture.load("lessons/" + language + ".ktouch.xml");
}

function initLessons(){
  gLessons = gLecture.getElementsByTagName("Level");

  // clear the level selector
  var menupopup = gDialog.level.getElementsByTagName("menupopup").item(0);
  try {
    gDialog.level.removeChild(menupopup);
  } catch(e) {}

  // fill the level selector drop-down menu
  menupopup = document.createElement("menupopup");
  var menuitem = null;
  var newChars = "";
  for (var i = 0; i < gLessons.length; i++) {
    newChars = gLessons[i].getElementsByTagName("NewCharacters").item(0).childNodes[0].nodeValue;
    menuitem = document.createElement("menuitem");
    menuitem.setAttribute("label", (i+1) + ": " + newChars);
    menuitem.setAttribute("oncommand", "newPromptFromLessons()");
    menupopup.appendChild(menuitem);
  }

  // select the first lesson
  gDialog.level.appendChild(menupopup);
  gDialog.level.selectedIndex = 0;
  newPromptFromLessons();
}


// Copied from http://members.aol.com/gulfhigh2/words9.html
//   see also: http://users.tinyonline.co.uk/gswithenbank/pangrams.htm
const gPangrams = [
  "A large fawn jumped quickly over white zinc boxes.",
  "A mad boxer shot a quick, gloved jab to the jaw of his dizzy opponent.",
  "A quart jar of oil mixed with zinc oxide makes a very bright paint.",
  "A quick brown fox jumps over the lazy dog.",
  "A quick movement of the enemy will jeopardize six gunboats.",
  "A very bad quack might jinx zippy fowls.",
  "About sixty codfish eggs will make a quarter pound of very fizzy jelly.",
  "Alfredo just must bring very exciting news to the plaza quickly.",
  "All questions asked by five watch experts amazed the judge.",
  "Amazingly few discotheques provide jukeboxes",
  "Anxious Paul waved back his pa from the zinc quarry just sighted.",
  "Astronaut Quincy B. Zack defies gravity with six jet fuel pumps.",
  "Back in June we delivered oxygen equipment of the same size.",
  "Back in my quaint garden, jaunty zinnias vie with flaunting phlox.",
  "Ban foul, toxic smogs which quickly jeopardize lives.",
  "Barkeep! A flaming tequila swizzle and a vodka and Ajax, hold the cherry.",
  "Baroque? Hell, just mix a dozen wacky pi fonts & you've got it.",
  "Bawds jog, flick quartz, vex nymph.",
  "Big July earthquakes confound zany experimental vow.",
  "Blowzy frights vex, and jump quick.",
  "Blowzy night-frumps vex'd Jack Q.",
  "Blowzy red vixens fight for a quick jump.",
  "Boy, Max felt hazy during his quick weaving jumps!",
  "Boys of quartz duck phlegm, vow jinx.",
  "Brawny gods just flocked up to quiz and vex him.",
  "Brick quiz whangs jumpy veldt fox.",
  "By Jove, my quick study of lexicography won a prize.",
  "Campus TV quiz: just why is gold buried at Fort Knox?",
  "Cozy lummox gives smart squid who asks for job pen.",
  "Cozy sphinx waves quart jug of bad milk.",
  "Crazy Fredericka bought many very exquisite opal jewels.",
  "Crux: Why joking TV blazes FM PDQ?",
  "Cwm, fjord-bank glyphs vext quiz",
  "Dangerously frozen, he quickly judged his extremities to be waterproof.",
  "Doxy with charming buzz quaffs vodka julep.",
  "Dr. Jekyll vows to finish zapping quixotic bum",
  "Dub waltz, nymph, for quick jigs vex.",
  "Dumpy kibitzer jingles as exchequer overflows.",
  "Emily Q. Jung-Schwartzkopf XV, B.D.",
  "Exquisite farm wench gives body jolt to prize stinker.",
  "Exquisite wizard flock behaving jumpy",
  "Fabled reader with jaded, roving eye seized by quickened impulse to expand budget.",
  "Few quips galvanized the mock jury box.",
  "Five big quacking zephyrs jolt my wax bed.",
  "Five jumbo oxen graze quietly with packs of dogs.",
  "Five or six big jet planes zoomed quickly by the tower.",
  "Five wine experts jokingly quizzed Chablis sample.",
  "Fjord-buck zags whelm qvint pyx.",
  'For only $49, jolly housewives made "inexpensive" meals using quick-frozen vegetables.',
  "Foxy Gen. Schwarzkopf jumbled Iraqi TV.",
  "Foxy nymphs grab quick-lived waltz.",
  "Fred specialized in the job of making very quaint wax toys.",
  "Freight to me sixty dozen quart jars and twelve black pans.",
  "Frowzy things plumb vex'd Jack Q.",
  "Frozen buyer just quickly keyed shocking weaver's $34,825,679 complexion.",
  "Frumpy veld wags quiz jack-in-the-box.",
  "G.W. Bush quickly fixed prize jam on TV.",
  "Glum Q. Schwarzkopf jinxed by TV.",
  "Glum, wavyhaired excon bequeaths fake topaz jewel.",
  "Grumpy wizards make toxic brew for the evil Queen and Jack.",
  "Guzzling of jaunty exile wrecks havoc at damp banquet.",
  "Gyps balk nth fjord cwm quiz, vex. (see note)",
  "Hark! Toxic jungle water vipers quietly drop on zebras for meals!",
  "Heavy boxes perform quick waltzes and jigs.",
  "Hick Jed wins quiz for extra blimp voyage.",
  "How jolly vexing a fumble to drop zucchini in the quicksand!",
  "How quickly daft jumping zebras vex.",
  "How razorback-jumping frogs can level six piqued gymnasts!",
  "How vexing a fumble to drop a jolly zucchini in the quicksand.",
  "I have quickly spotted the four women dozing in the jury box.",
  "I jump quickly, shaving two dozen fox bare.",
  "I was temporarily forced to zig-zag and quiver furiously around big junky xylophones.",
  "If Jack quiz bald nymphs grow vext.",
  "In Boston: Jazzy (but malicious) squeak toy explodes, ending the lives of two children.",
  'J. Hoefler cabled: "puzzling over waxy kumquats."',
  "J.Q. Schwartz flung D V Pike my box",
  "J.Q. Vandz struck my big fox whelp.",
  "Jack Farmer realized that the big yellow quilts were expensive.",
  "Jack amazed a few girls by dropping the antique onyx vase!",
  "Jack believed we quiz sphinx from Egypt.",
  "Jack-in-the-box quiz fed grumpy wolves.",
  "Jackdaws love my big sphinx of quartz.",
  "Jaded zombies acted quaintly but kept driving their oxen forward.",
  "Jail zesty vixen who grabbed pay from quack.",
  "Jay visited back home and gazed upon a brown fox and quail.",
  "Jeb quickly drove a few extra miles on the glazed pavement.",
  "Jim just quit and packed extra heavy bags for Liz Owen.",
  "Jimmy and Zack, the police explained, were last seen diving into a field of buttered quahogs.",
  "Jink cwm, zag veldt, fob qursh pyx.",
  "Jocks fix BMW, vend quartz glyph.",
  "Jolly few pangrams vanquished by exotic kudzu.",
  "Jolly housewives made inexpensive meals using quick-frozen vegetables.",
  "Judge Powell quickly gave six embezzlers stiff sentences.",
  "Judges vomit; few quiz pharynx block.",
  "Jumbling vext frowzy hacks PDQ.",
  "Jump by vow of quick, lazy strength in Oxford.",
  "Jumpy wizard quit having black foxes.",
  "Jumpy zebra vows to quit thinking coldly of sex.",
  "Junky qoph-flags vext crwd zimb.",
  "Just be very quick when fixing zip code mail.",
  "Just keep examining every low bid quoted for zinc etchings.",
  "Just work for improved basic techniques to maximize your typing skill.",
  "King Alexander was just partly overcome after quizzing Diogenes in his tub.",
  "Lazy jackal from raiding xebec prowls the quiet cove.",
  "Lazy movers quit hard packing of jewelry boxes.",
  "Many big jackdaws quickly zipped over the fox pen.",
  "Many-wived Jack laughs at probes of sex quiz.",
  "Martin J. Hixeypovzer quickly began his first word.",
  "May Jo equal the fine record by solving six puzzles a week?",
  "Meg Schwarzkopf quit Jynx Blvd.",
  "Meghan deftly picks valuable jewels: onyx, quartz.",
  "Milk-vat fez bugs qoph-crwd jynx.",
  "Mix Zapf with Veljovic and get quirky beziers.",
  "Monique, the buxom coed, likes to fight for Pez with the junior varsity team.",
  "Mr. Jock, TV quiz Ph.D., bags few lynx.",
  "Murky haze enveloped a city as jarring quakes broke forty-six windows.",
  "My faxed joke won a pager in the cable TV quiz show.",
  "My girl wove six dozen plaid jackets before she quit.",
  "My grandfather picks up quartz and valuable onyx jewels.",
  "My help squeezed back in again after six and joined the weavers.",
  "My help squeezed in and joined the weavers again before six oclock.",
  "My kind zap Fox TV, squelch GWB Jr.!",
  "NBC glad. Why? Fox TV jerks quiz PM.",
  "Nancy Bizal exchanged vows with Robert J. Kumpf at Quincy Temple.",
  "New job: fix Mr. Gluck's hazy TV, PDQ!",
  "Now is the time for all quick brown dogs to jump over the lazy lynx.",
  "Nth balks gyp, vex cwm fjord quiz.",
  "Nth black fjords vex Qum gyp wiz.",
  "Nth quark biz gyps cwm fjeld vox.",
  "Nth zigs block Qum dwarf jive pyx.",
  "On the boardwalk grave playful lizards quickly jump and exercise.",
  "Oozy quivering jellyfish expectorated by mad hawk.",
  "Pack my box with five dozen liquor jugs.",
  'Pangrams have subjects like "dewy fox quiz".',
  "Perhaps President Clinton's amazing sax skills will be judged quite favorably.",
  "Phlegms fyrd wuz qvint jackbox.",
  "Picking just six quinces, new farm hand proves strong but lazy.",
  "Pickled, Gorbachev jumps tawny fax quiz.",
  "Pink foxglove bouquet charms dizzy Jew.",
  "Playing jazz vibe chords quickly excites my wife.",
  "Poxy G-men quiz FDR; LBJ whacks TV!",
  "Prized waxy jonquils choke big farm vats.",
  "Prodigal lesbians from Venezuela know just exactly how to eat quiche.",
  "Puzzled women bequeath jerks with very exotic gifts.",
  "Pyx crwth fjeld, quok, vang, zimbs.",
  "Pyx vang quiz: komb crwth fjelds.",
  "Quartz glyph job vex'd cwm finks.",
  "Quartz glyph jocks vend, fix BMW.",
  "Questions of a zealous nature have become by degrees petty waxen jokes.",
  "Quick fawns jumped over a lazy dog.",
  "Quick jigs for waltz vex bad nymph.",
  "Quick wafting zephyrs vex bold Jim.",
  "Quick waxy bugs jump the frozen veldt.",
  "Quick zephyrs blow, vexing daft Jim.",
  "Quiet guys broke zip files with my coven’s jade ax.",
  '"Quit beer," vows dizzy, puking, Michael J. Fox',
  "Quixotic Republicans vet first key zero-growth jeremiad.",
  "Quixotic knights' wives are found on jumpy old zebras.",
  "Quiz explained for TV show by Mick Jagger.",
  "Qursh gowf veldt jynx zimb pack.",
  "Raving zibet chewed calyx of pipsqueak major.",
  "Sex prof gives back no quiz with mild joy.",
  "Six big devils from Japan quickly forgot how to waltz.",
  "Six big juicy steaks sizzled in a pan as five workmen left the quarry.",
  "Six boys guzzled cheap raw plum vodka quite joyfully.",
  "Six crazy kings vowed to abolish my quite pitiful jousts.",
  "Six javelins thrown by the quick savages whizzed forty paces beyond the mark.",
  "Six of the women quietly gave back prizes to the judge.",
  "Six plump boys guzzled cheap raw vodka quite joyfully.",
  "Sixty zippers were quickly picked from the woven jute bag.",
  "Sixty-five wildly panting fruitflies gazed hungrily at the juicy bouncing kumquats.",
  "Sphinx of black quartz: judge my vow.",
  "Squdgy fez, blank jimp crwth vox.",
  "Suez sailor vomits jauntily abaft while waxing parquet decks.",
  "Sympathizing would fix Quaker objectives.",
  "The July sun caused a fragment of black pine wax to ooze on the velvet quilt.",
  "The July sun causes black pine wax fragments to ooze on velvet quilt.",
  "The black quartz lynx was a jivy imp of God.",
  "The chancellor of the exchequer embezzled junk from gypsies with vigor.",
  "The exodus of jazzy pigeons is craved by squeamish walkers.",
  "The explorer was frozen in his big kayak just after making queer discoveries.",
  "The five boxing wizards jump quickly.",
  "The jay, pig, fox, zebra, and my wolves all quack.",
  "The job of waxing linoleum frequently peeves chintzy kids.",
  "The job requires extra pluck and zeal from every young wage earner.",
  "The jukebox music puzzled a gentle visitor from a quaint valley town.",
  "The public was amazed to view the quickness and dexterity of the juggler.",
  "The quick brown fox jumps over a lazy dog.",
  "The risque gown marked a very brazen exposure of juicy flesh.",
  "The sex life of the woodchuck is a provocative question for most vertebrate zoology majors.",
  "The sixty-five quickly jumping dogs and foxes were just shot by the crazy maniac.",
  "The vixen jumped quickly on her foes barking with zeal.",
  "The xylophone orchestra vowed to imbibe jugs of kumquat fizz.",
  "Their kind aunt was subject to frequent dizzy spells, thus causing much anxiety and worry.",
  "Thumb frowzly, vexing jacks, PDQ.",
  "Travelling beneath the azure sky in our jolly ox-cart, we often hit bumps quite hard.",
  "Turgid saxophones blew over Mick's jazzy quiff.",
  "Two hardy boxing kangaroos jet from Sydney to Zanzibar on quicksilver pinions.",
  "Two joyful vixens squirt milk upon the caged zebra.",
  "Up at the zoo a roving ox was quickly fed bug jam.",
  "Uphill jogging will tax pounding heart muscle very quickly; better to be lazy!",
  "Verbatim reports were quickly given by Jim Fox to his amazed audience.",
  "Verily the dark ex-Jew quit Zionism, preferring the cabala.",
  "Vexed funky camp juggler quit show biz.",
  "Victors flank gypsy who mixed up on job quiz.",
  "Viewing quizzical abstracts mixed up hefty jocks.",
  "Viewing quizzical abstracts mixed up hefty jocks.",
  "Waltz, dumb nymph, for quick jigs vex.",
  "Waltz, nymph, for quick jigs vex bud.",
  "Waqf vozhd trecks, jumbling pyx.",
  "Was there a quorum of able whizzkids gravely exciting the jaded fish at ATypI?",
  "Watch all five questions asked by experts amaze the judge.",
  "Wavy Jake's fat zebra had Mexican pig liquor.",
  "We could jeopardize six of the gunboats by two quick moves.",
  "We crazed folk quit having sex, be jumpy.",
  "We dislike to exchange job lots of sizes varying from a quarter up.",
  "We have just quoted on nine dozen boxes of gray lamp wicks.",
  "We promptly judged antique ivory buckles for the next prize.",
  "We quickly chased the zebras to a quiet drinking place just near my five bundles of flax.",
  "We quickly seized the black axle and just saved it from going past him.",
  "Weekly magazines request help for and by junior executives.",
  "West quickly gave Bert handsome prizes for six juicy plums.",
  "Wham! Volcano erupts fiery liquid death onto ex-jazzbo Kenny G.",
  "When waxing parquet decks, Suez sailors vomit jauntily abaft.",
  "When we go back to Juarez, Mexico, do we fly over picturesque Arizona?",
  "Whenever the black fox jumped the squirrel gazed suspiciously.",
  "While making deep excavations we found some quaint bronze jewelry.",
  "Why jab, vex quartz-damping flocks?",
  "Will Major Douglas be expected to take this true-false quiz very soon?",
  "William Jex quickly caught five dozen Republicans.",
  "Wolves exit quickly as fanged zoo chimp jabbers.",
  "Worn DJ: vex Gk HQ, clamp fubsy zit.",
  "Would you please examine both sizes of jade figures very quickly.",
  "Woven silk pyjamas exchanged for blue quartz.",
  "XV quick nymphs beg fjord-waltz.",
  "Xavier picked bright yellow jonquils for Mitzi.",
  "Xylophone wizard begets quick jive form.",
  "You go tell that vapid existentialist quack Freddy Nietzsche that he can just bite me, twice.",
  "You will quite often be amazed by Jock's very weighty, size six sporran!",
  "Zany Jacques French's pub mixed watery vodka gimlets.",
  "Zelda quickly wove eight nubby flax jumpers."
];

// http://www.fatrazie.com/pangramme.htm
// Voir aussi la liste Oulipo, référence absolue de ce genre d'exercices littéraires ! ;-)
const gPangrams_fr = [
  "Aux wisigoths : voyez Frank, Jacques, Pablo à midi.",
  "Bidoche qui fume ? Volkswagen : partez joyeux !",
  "Bloquez dix wharfs, Viking, j'y compte !",
  "Body qui fume chez Volkswagen : juste prix ?",
  "Body qui fume chez Volkswagen : prix jet ?",
  "Breitschwanz : vieux pelage yak que je modifie.",
  "Breitschwanz piqué, moujik yeux flagada, vu ?",
  "Bâchez la queue du wagon-taxi avec les pyjamas du fakir.",
  "Coq, jugez d'humbles wax krypton vif.",
  "D'ubac, fi, morve ! Jeux, zython, kwas pélagique !",
  "Dauw Hébé : jugez l'aptéryx (vif coq) à Minsk.",
  "Dax, Zwicky flambant phoque gras, java...",
  "Dix kilos que Perec, zouave, y juge 'bof' : what a man !",
  "Douze à six au hockey, faut que je me prive au bowling.",
  "Drawback sympathique ! Gonflez joviaux !",
  "Duquez patchwork ! J'y flambe vingt-six !",
  "Défalquez-y vingt-six jambes de ce patchwork.",
  "Enjambez l'exquis patchwork du goy vif.",
  "Excusez l'interview du morphing que fit la web-jockey.",
  "Exhibez votre k-way disloqué, poncif méjugé.",
  "Extraconjugal whisky, pique-boeuf, vidimez !",
  "F Haine : Mégret y vide jews ou bock ? Expliquez.",
  "Faquir pompeux, vidangez l'abject whisky.",
  "Faux kwachas ? Quel projet de voyage zambien !",
  "Faux kwachas ? Quel projet de voyage zambien !",
  "Fi d'ubac pélagique, morveux ! Jeu, zython, kwas !",
  "Film VHS bat Woyzeck quand j'expurge.",
  "Flambez patchwork ! J'y duque vingt-six !",
  "Flushing-Meadow, Quebec, Metz, Reykjavik, ..., Paix !",
  "Flux : Zwicky, jadis gravement phobique.",
  "Fougueux, j'enivre la squaw au pack de beau zythum.",
  "Fripon, mixez l'abject whisky qui vidange.",
  "Fritz Hemingway, vieux bijou kaléidoscopique.",
  "Fumeux, phagédénique... Objectivez, walkyries !",
  "Humble époux, fardez vingt squaw jockey.",
  "J'ai bu kwas. Chien vida aptéryx moquez golf.",
  "J'ai bu vif gin. Masquez wok. Laché d'aptéryx.",
  "J'exhibe qui ? Woyzeck ? Soldat pur ? Vif gamin ?",
  "Jacky, zouave ébahi, un steward ? Explique-moi, GEF !",
  "Jeux : trafiquez ! Vainc, whisky ! Déplombage !",
  "Job lynx, grimpez vif ketch de squaw.",
  "Jouvenceaux, trafiquez : déplombage, whisky !",
  "Judex vainc : trafiquez plombage whisky !",
  "Juge, flambez l'exquis patchwork d'Yvon.",
  "Jugez qu'un vieux whisky blond pur malt fonce.",
  "Jugez vite faux whisky blond parmi cinq.",
  "Ketch, yawl, jonque flambant neuve… jugez des prix !",
  "Kiwi ? Figue ? Débloquez vite ce pharynx, James.",
  "Kiwi fade, aptéryx, quel jambon vous gâchez !",
  "Kiwi, aptéryx, juge blond qui fume chez vous ?",
  "Le moujik équipé de faux breitschwanz voyage.",
  "Lynch  IVG ; zoom pub fjord ; kwas : toxique !",
  "Magnifique Walbrzych juxtapose vodka.",
  "Magnifique walkyrie, objectivez hexapodes.",
  "Ming que j'exhibe : pur Woyzeck ? soldat vif ?",
  "Mkrtchyan, axez joug vip waqfs bled.",
  "Mon pauvre zébu ankylosé choque deux fois ton wagon jaune.",
  "Moujik, qui pèle de faux breitschwanz, voyage.",
  'New-yorkais hémiplégiques, objectivez "féodaux" !',
  "Ô juteux bavardage pacifique. Why ? Zalmanski !",
  "Ô pudique moujik, bâclez-y vingt-six wharfs !",
  "Objectif : exigez un k-way de marque Elvis, hop !",
  "Objectif gravidique : asphyxiez walkman !",
  "Objective Walkyrie ! Niquez aghas , PD fumeux !",
  "Patchwork : défalquez-y vingt-six jambes.",
  "Patchwork de vingt-six, quel job ! Fumez-y !",
  "Patchwork funambulesque : divaguez joyeux !",
  "Pauvre soldat Woyzeck. Magnifique ! J'exhibe...",
  "Perchez dix, vingt woks. Qu'y flambé-je ?",
  "Planchez votre fameux k-way basque : je guide.",
  "Portez au juge cinq bols de vos fameux whisky.",
  "Portez ce vieux whisky au juge blond qui fume.",
  "Pour cinq ou dix bizuts, je file au voyage mohawk !",
  "Prix flagada, j'abecque: vin, kwas, zythum.",
  "Prouvez, beau juge, que le fameux sandwich au yak tue.",
  "Provoquez Bydlowski et je change : fameux !",
  "Pub : jugez-vous faux ce tomahawk cylindrique ?",
  "Pyjama de faux breitschwanz ? Logique à Kiev !",
  "Quand film X (VHS) « Woyzeck » : big projet !",
  "Quand j'exhorte Woyzeck  : pige, flambe, vis !",
  "Quand sage j'exhorte Woyzeck : plomb vif !",
  "Que faire du joli wapiti viking ? Bouchez mes yeux !",
  "Que vingt-six bulldozers forment joyeux patchwork.",
  "Quid job vingt-six ? Filmez-y patchwork !",
  "Quid jobs ? Wolf ? Gap : vexez Mkrtchyan !",
  "TV Fox : Bydlowski, jonque, gaz camphré.",
  "Tremblez, joyeux whig paf : cinq vodkas !",
  "Un mot jugé vrai/faux piqué chez Bydlowski.",
  "Vague ex-tadjik, plombez-y cinq wharfs !",
  "Vexez psy à Ulm. Bon jerk à Wight. CQFD.",
  "Vidéographique zwieback ! Flamants joyeux !",
  "Vieux flegme bio ce week-end : j'astique au zéphyr.",
  "Vieux parking Schwyz table que je modifie.",
  "Vieux pelage que je modifie : breitschwanz ou yak?",
  "Vif P-DG mentor, exhibez la squaw jockey.",
  "Vif juge, trempez ce blond whisky aqueux.",
  "Voix ambiguë d'un cœur qui, au zéphyr, préfère les jattes de kiwis.",
  "Vous jugez bien plus fameux ce quart de whisky.",
  "Voyez ce jeu exquis wallon, de graphie en kit mais bref.",
  "Voyez le brick géant que j'examine près du wharf.",
  "Walkyrie, adjugez pacifiquement vos hiboux.",
  "Wharf tadjik au plomb y vexe cinq zigs.",
  "Wharf tadjik, plomb, vexez-y cinq gus !",
  "Wharfs, plomb : coq tadjik vexé y zingue.",
  "Whisky abject qui gonflez dix vampires.",
  "Whisky vert : jugez cinq fox d'aplomb.",
  "Y'a déjà mieux que breitschwanz, Gef : volapük !",
  "Zweig : psychonévrotique ex-judoka flambé !",
  "Zweig modifia le juke-box psychonévrotique.",
  "Zython je veux, kwas ! Camp bref d'algique !"
];

