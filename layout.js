// Copyright 2007 Fabien Cazenave, all rights reserved.

var gKeymap = new Array();
var gKeymod = new Array();

var gLayout = "";          // active layout
var gLayouts  = Array();   // xml documents
var gVariant = "";         // active variant
var gVariants = Array();   // xml nodes

function loadLayouts(layout, variant) {
  gVariant = variant;
  gLayout = layout;
  gLayouts[layout] = document.implementation.createDocument("", "", null);
  gLayouts[layout].onload = onLoadLayouts;
  gLayouts[layout].load("layouts/" + layout + ".xml");
}

function onLoadLayouts() {
  gVariants = gLayouts[gLayout].getElementsByTagName("variant");
  var layout = gLayouts[gLayout].getElementsByTagName("title")
                       .item(0).childNodes[0].nodeValue;

  // fill the layout selector drop-down menu if needed
  var menupopup = document.getElementById("layout-" + layout);
  if (!menupopup.hasChildNodes()) {
    var menuitem = null;
    var variant = "";
    for (var i = 0; i < gVariants.length; i++) {
      if (gVariants[i].hasAttribute("separator")) {
        menuitem = document.createElement("menuseparator");
      } else {
        variant = gVariants[i].getAttribute("id");
        menuitem = document.createElement("menuitem");
        //menuitem.setAttribute("label", layout + " (" + variant + ")");
        menuitem.setAttribute("label", variant);
        menuitem.setAttribute("oncommand", "onSelectLayout('" + layout + "', '" + variant + "')");
      }
      menupopup.appendChild(menuitem);
    }
  }

  // select the layout, if any
  if (gVariant && gVariant.length)
    onSelectLayout(gLayout, gVariant);
}

function onSelectLayout(layout, variant) {
  //var sel = gLayouts.getElementById(variant);
  // getElementById doesn't work on these XML files and I can't see why *sigh*
  // So this here's a dirty getElementById:
  var tmp = gLayouts[layout].getElementsByTagName("variant");
  for (var i=0; i<tmp.length; i++) {
    var sel = tmp[i];
    if (sel.getAttribute("id") == variant) break;
  }
  
  // load the base layout the selected variant relies on, if any
  if (sel.hasAttribute("include"))
    onSelectLayout(layout, sel.getAttribute("include"));

  // update the selector
  tmp = document.getElementById("layout-" + layout).parentNode;
  i = 0;
  while(tmp = tmp.previousSibling) i--;
  gDialog.layout.selectedIndex = -i;
  gDialog.variant.value = variant;

  // fill the graphical keyboard
  var key = null;
  var keys = sel.getElementsByTagName("key");
  for (var i=0; i<keys.length; i++) {
    key = keys[i];
    setKey(key.getAttribute("name"), key.getAttribute("base"), key.getAttribute("shift"));
  }
}

function setKey(code, normal, shift) {
  var key = document.getElementById(code);
  key.setAttribute("value", normal.toUpperCase());
  gKeymap[normal] = key;
  gKeymap[shift]  = key;

  gKeymod[normal] = null;
  var hand = key.getAttribute("class");
  if (hand.substr(0, 4) == "left")
    gKeymod[shift] = gDialog.rShiftKey;
  else if (hand.substr(0, 5) == "right")
    gKeymod[shift] = gDialog.lShiftKey;
  else
    gKeymod[shift] = null;
}

function setDeadKey(code, normal, shift) {
  document.getElementById(code).setAttribute("value", normal);
}

function setSpecialKey(code, value) {
}

// QWERTY
function switchKeyboardLayoutQWERTY() {
  // row #1
  setKey('TLDE', '`', '~');
  setKey('AE01', '1', '!');
  setKey('AE02', '2', '@');
  setKey('AE03', '3', '#');
  setKey('AE04', '4', '$');
  setKey('AE05', '5', '%');
  setKey('AE06', '6', '^');
  setKey('AE07', '7', '&');
  setKey('AE08', '8', '*');
  setKey('AE09', '9', '(');
  setKey('AE10', '0', ')');
  setKey('AE11', '-', '_');
  setKey('AE12', '=', '+');

  // row #2
  setKey('AD01', 'q', 'Q');
  setKey('AD02', 'w', 'W');
  setKey('AD03', 'e', 'E');
  setKey('AD04', 'r', 'R');
  setKey('AD05', 't', 'T');
  setKey('AD06', 'y', 'Y');
  setKey('AD07', 'u', 'U');
  setKey('AD08', 'i', 'I');
  setKey('AD09', 'o', 'O');
  setKey('AD10', 'p', 'P');
  setKey('AD11', '[', '{');
  setKey('AD12', ']', '}');
  setKey('BKSL', '\\', '|');

  // row #3
  setKey('AC01', 'a', 'A');
  setKey('AC02', 's', 'S');
  setKey('AC03', 'd', 'D');
  setKey('AC04', 'f', 'F');
  setKey('AC05', 'g', 'G');
  setKey('AC06', 'h', 'H');
  setKey('AC07', 'j', 'J');
  setKey('AC08', 'k', 'K');
  setKey('AC09', 'l', 'L');
  setKey('AC10', ';', ':');
  setKey('AC11', "'", '"');
  setKey('EUR1', '\\', '|');

  // row #4
  setKey('EUR2', '<', '>');
  setKey('AB01', 'z', 'Z');
  setKey('AB02', 'x', 'X');
  setKey('AB03', 'c', 'C');
  setKey('AB04', 'v', 'V');
  setKey('AB05', 'b', 'B');
  setKey('AB06', 'n', 'N');
  setKey('AB07', 'm', 'M');
  setKey('AB08', ',', '<');
  setKey('AB09', '.', '>');
  setKey('AB10', '/', '?');

  // special keys
  setKey('SPCE', ' ', ' ');
  setSpecialKey(022, 'Backspace');
  setSpecialKey(023, 'Tab');
  setSpecialKey(066, 'Caps');
  setSpecialKey(000, 'Enter');
  setSpecialKey(050, 'Shift');
  setSpecialKey(062, 'Shift');
}

// QWERTZ
function switchKeyboardLayoutQWERTZ() {
  switchKeyboardLayoutQWERTY();
  // row #1
  setKey('TLDE', '^', '°');
  setKey('AE02', '2', '"');
  setKey('AE03', '3', '§');
  setKey('AE06', '6', '&');
  setKey('AE07', '7', '/');
  setKey('AE08', '8', '(');
  setKey('AE09', '9', ')');
  setKey('AE10', '0', '=');
  setKey('AE11', 'ß', '?');
  setDeadKey('AE12', "'", '`');
  // row #2
  setKey('AD06', 'z', 'Z');
  setKey('AD11', 'ü', 'Ü');
  setKey('AD12', '+', '*');
  setKey('BKSL', '#', "'");
  // row #3
  setKey('AC10', 'ö', 'Ö');
  setKey('AC11', "ä", 'Ä');
  setKey('EUR1', '#', "'");
  // row #4
  setKey('EUR2', '<', '>');
  setKey('AB01', 'y', 'y');
  setKey('AB08', ',', ';');
  setKey('AB09', '.', ':');
  setKey('AB10', '-', '8');
}

// AZERTY
function switchKeyboardLayoutAZERTY() {
  switchKeyboardLayoutQWERTY();
  // row #1
  setKey('TLDE', '²', ' ');
  setKey('AE01', '&', '1');
  setKey('AE02', 'é', '2');
  setKey('AE03', '"', '3');
  setKey('AE04', "'", '4');
  setKey('AE05', '(', '5');
  setKey('AE06', '-', '6');
  setKey('AE07', 'è', '7');
  setKey('AE08', '_', '8');
  setKey('AE09', 'ç', '9');
  setKey('AE10', 'à', '0');
  setKey('AE11', ')', '°');
  setKey('AE12', '=', '+');
  // row #2
  setKey('AD01', 'â', 'Â');
  setKey('AD01', 'a', 'A');
  setKey('AD02', 'z', 'Z');
  setKey('AD03', 'ê', 'Ê');
  setKey('AD03', 'ë', 'Ë');
  setKey('AD03', 'e', 'E');
  setDeadKey('AD11', '^', '"');
  setKey('AD12', '$', '£');
  setKey('BKSL', '*', 'µ');
  // row #3
  setKey('AC01', 'q', 'Q');
  setKey('AC10', 'm', 'M');
  setKey('AC11', 'ù', '%');
  setKey('EUR1', '*', 'µ');
  // row #4
  setKey('EUR2', '<', '>');
  setKey('AB01', 'w', 'W');
  setKey('AB07', ',', '?');
  setKey('AB08', ';', '.');
  setKey('AB09', ':', '/');
  setKey('AB10', '!', '§');
}

// Colemak
function switchKeyboardLayoutCOLEMAK() {
  switchKeyboardLayoutQWERTY();

  // row #2
  setKey('AD03', 'f', 'F');
  setKey('AD04', 'p', 'P');
  setKey('AD05', 'g', 'G');
  setKey('AD06', 'j', 'J');
  setKey('AD07', 'l', 'L');
  setKey('AD08', 'u', 'U');
  setKey('AD09', 'y', 'Y');
  setKey('AD10', ';', ':');

  // row #3
  setKey('AC02', 'r', 'R');
  setKey('AC03', 's', 'S');
  setKey('AC04', 't', 'T');
  setKey('AC05', 'd', 'D');
  setKey('AC07', 'n', 'N');
  setKey('AC08', 'e', 'E');
  setKey('AC09', 'i', 'I');
  setKey('AC10', 'o', 'O');

  // row #4
  setKey('AB06', 'k', 'K');
}

// Dvorak
function switchKeyboardLayoutDVORAK() {
  // row #1
  setKey('TLDE', '`', '~');
  setKey('AE01', '1', '!');
  setKey('AE02', '2', '@');
  setKey('AE03', '3', '#');
  setKey('AE04', '4', '$');
  setKey('AE05', '5', '%');
  setKey('AE06', '6', '^');
  setKey('AE07', '7', '&');
  setKey('AE08', '8', '*');
  setKey('AE09', '9', '(');
  setKey('AE10', '0', ')');
  setKey('AE11', '[', '{');
  setKey('AE12', ']', '}');

  // row #2
  setKey('AD01', "'", '"');
  setKey('AD02', ',', '<');
  setKey('AD03', '.', '>');
  setKey('AD04', 'p', 'P');
  setKey('AD05', 'y', 'Y');
  setKey('AD06', 'f', 'F');
  setKey('AD07', 'g', 'G');
  setKey('AD08', 'c', 'C');
  setKey('AD09', 'r', 'R');
  setKey('AD10', 'l', 'L');
  setKey('AD11', '/', '?');
  setKey('AD12', '=', '+');
  setKey('BKSL', '\\', '|');

  // row #3
  setKey('AC01', 'a', 'A');
  setKey('AC02', 'o', 'O');
  setKey('AC03', 'e', 'E');
  setKey('AC04', 'u', 'U');
  setKey('AC05', 'i', 'I');
  setKey('AC06', 'd', 'D');
  setKey('AC07', 'h', 'H');
  setKey('AC08', 't', 'T');
  setKey('AC09', 'n', 'N');
  setKey('AC10', 's', 'S');
  setKey('AC11', '-', '_');
  setKey('EUR1', '\\', '|');

  // row #4
  setKey('EUR2', '<', '>');
  setKey('AB01', ';', ':');
  setKey('AB02', 'q', 'Q');
  setKey('AB03', 'j', 'J');
  setKey('AB04', 'k', 'K');
  setKey('AB05', 'x', 'X');
  setKey('AB06', 'b', 'B');
  setKey('AB07', 'm', 'M');
  setKey('AB08', 'w', 'W');
  setKey('AB09', 'v', 'V');
  setKey('AB10', 'z', 'Z');

  // special keys
  setKey('SPCE', ' ', ' ');
  setSpecialKey(022, 'Backspace');
  setSpecialKey(023, 'Tab');
  setSpecialKey(066, 'Caps');
  setSpecialKey(000, 'Enter');
  setSpecialKey(050, 'Shift');
  setSpecialKey(062, 'Shift');
}

// Dvorak-fr (Bepo)
function switchKeyboardLayoutBEPO() {
  // row #1
  setKey('TLDE', '$', '#');
  setKey('AE01', '"', '1');
  setKey('AE02', '«', '2');
  setKey('AE03', '»', '3');
  setKey('AE04', "(", '4');
  setKey('AE05', ')', '5');
  setKey('AE06', '@', '6');
  setKey('AE07', '+', '7');
  setKey('AE08', '-', '8');
  setKey('AE09', '/', '9');
  setKey('AE10', '*', '0');
  setKey('AE11', '=', '^');
  setKey('AE12', '%', '`');

  // row #2
  setKey('AD01', "b", 'B');
  setKey('AD02', 'é', 'É');
  setKey('AD03', 'p', 'P');
  setKey('AD04', 'o', 'O');
  setKey('AD05', 'è', 'È');
  setKey('AD06', '^', '!');
  setKey('AD07', 'v', 'V');
  setKey('AD08', 'd', 'D');
  setKey('AD09', 'l', 'L');
  setKey('AD10', 'j', 'J');
  setKey('AD11', 'z', 'Z');
  setKey('AD12', 'w', 'W');
  setKey('BKSL', 'ç', 'Ç');

  // row #3
  setKey('AC01', 'a', 'A');
  setKey('AC02', 'u', 'U');
  setKey('AC03', 'i', 'I');
  setKey('AC04', 'e', 'E');
  setKey('AC05', ',', '?');
  setKey('AC06', 'c', 'C');
  setKey('AC07', 't', 'T');
  setKey('AC08', 's', 'S');
  setKey('AC09', 'r', 'R');
  setKey('AC10', 'n', 'N');
  setKey('AC11', 'm', 'M');
  setKey('EUR1', 'ç', 'Ç');

  // row #4
  setKey('EUR2', 'ê', 'Ê');
  setKey('AB01', 'x', 'X');
  setKey('AB02', 'y', 'Y');
  setKey('AB03', 'à', 'À');
  setKey('AB04', '.', ':');
  setKey('AB05', 'k', 'K');
  setKey('AB06', "'", ';');
  setKey('AB07', 'q', 'Q');
  setKey('AB08', 'g', 'G');
  setKey('AB09', 'h', 'H');
  setKey('AB10', 'f', 'F');

  // special keys
  setKey('SPCE', ' ', ' ');
  setSpecialKey(022, 'Backspace');
  setSpecialKey(023, 'Tab');
  setSpecialKey(066, 'Caps');
  setSpecialKey(000, 'Enter');
  setSpecialKey(050, 'Shift');
  setSpecialKey(062, 'Shift');
}

// All
function switchKeyboardLayout(layout) {
  switch(layout) {
    case "dvorak":
      switchKeyboardLayoutDVORAK();
      break;

    case "bepo":
      switchKeyboardLayoutBEPO();
      break;

    case "qwerty":
      switchKeyboardLayoutQWERTY();
      break;

    case "qwertz":
      switchKeyboardLayoutQWERTZ();
      break;

    case "azerty":
      switchKeyboardLayoutAZERTY();
      break;

    case "colemak":
      switchKeyboardLayoutCOLEMAK();
      break;
  }
  highlightKey(gDialog.currKey);
  gDialog.txtAnswer.focus();
}

