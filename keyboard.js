// Copyright 2007 Fabien Cazenave, all rights reserved.
// Modifications: - 2009 Jean-David Maillefer
const nbRequiredSuccesses = 3;
const minSpeed = 150; // cpm
const maxTypos = 3; // %

const modLESSONS = 0;
const modPANGRAMS = 1;
const modCLIPBOARD = 2;

const defaultKeyboardStyle = "standard"; // typematrix, us
const defaultKeyboardLayout = "dvorak"; // azerty, qwertz, ...
const defaultKeyboardLayoutVariant = "fr-dvorak-bépo"; // nullable
const defaultLesson = "bepo";//"fr-dvorak-bepo-3";//
const defaultMode = modLESSONS;

var gDialog = {
	currKey : null,
	lastKey : null,
	lastMod : null,
	lShiftKey : null,
	rShiftKey : null,
	bspaceKey : null,
	txtPrompt : null,
	txtAnswer : null,
	keyboard : null,
	layout : null,
	level : null,
	mode : null,
	// timer
	wpmRecent : null,
	cpmRecent : null,
	errRecent : null,
	wpmCurrent : null,
	cpmCurrent : null,
	errCurrent : null,
	wpmAverage : null,
	cpmAverage : null,
	errAverage : null
/*
 * learning mode modeClipboard : false, modePangrams : false, modeLessons :
 * false
 */
};

var gTimer = {
	//  members
	timeStart : 0,
	timeStop : 0,
	nbErrors : 0,
	nbTries : 0,
	wpmSum : 0,
	cpmSum : 0,
	errSum : 0,
	wpmArr : null,
	cpmArr : null,
	errArr : null,

	// methods
	start : function() {
		// start counter
	var tmp = new Date();
	this.timeStart = tmp.getTime();
	this.nbErrors = 0;
	gDialog.txtPrompt.removeAttribute("class");
	},
	stop : function() {
		// stop counter
		var tmp = new Date();
		this.timeStop = tmp.getTime();
		gDialog.txtPrompt.setAttribute("class", "idle");
	},
	typo : function() {
		// increment error counter
		if (gDialog.txtAnswer.getAttribute("class") != "error")
			this.nbErrors++;
	},
	report : function(saveReport) {
		// number of words in the prompt
		var str = gDialog.txtAnswer.value;
		var nbWords = 0;
		var pos = str.indexOf(" ");
		while (pos != -1) {
			nbWords++;
			pos = str.indexOf(" ", pos + 1);
		}
		if (nbWords > 0 && str.charAt(str.length-1) != " ")
			nbWords++;
	
		// Display current stats
		var timeStop = saveReport ? this.timeStop : new Date().getTime();
		var elapsed = (timeStop - this.timeStart) / 60000; // time in minutes
		var cpm = Math.round(str.length / elapsed); // Characters Per Minute
		var wpm = Math.round(10 * nbWords / elapsed) / 10; // Words Per Minute
		var err = Math.round(1000 * this.nbErrors / str.length) / 10; // % of mistakes
		
	
		if (saveReport) {
			
			gDialog.wpmCurrent.value = wpm + " wpm";
			gDialog.cpmCurrent.value = cpm + " cpm";
			gDialog.errCurrent.value = this.nbErrors + " typo(s)";
			
			// Compute average over the last recent tries
			this.nbTries++;
			this.wpmArr.push(wpm);
			this.cpmArr.push(cpm);
			this.errArr.push(err);
			if (this.nbTries > nbRequiredSuccesses) {
				this.wpmArr.shift();
				this.cpmArr.shift();
				this.errArr.shift();
			}
			gDialog.wpmRecent.value = average(this.wpmArr, 1) + " wpm";
			gDialog.cpmRecent.value = average(this.cpmArr, 0) + " cpm";
			gDialog.errRecent.value = average(this.errArr, 1) + " %";

			// Display average stats
			this.wpmSum += wpm;
			this.cpmSum += cpm;
			this.errSum += err;
			cpm = Math.round(this.cpmSum / this.nbTries);
			wpm = Math.round(this.wpmSum * 10 / this.nbTries) / 10;
			err = Math.round(this.errSum * 10 / this.nbTries) / 10;
			gDialog.wpmAverage.value = wpm + " wpm";
			gDialog.cpmAverage.value = cpm + " cpm";
			gDialog.errAverage.value = err + " %";
		} else if (nbWords > 1) {
			gDialog.wpmCurrent.value = wpm + " wpm";
			gDialog.cpmCurrent.value = cpm + " cpm";
			gDialog.errCurrent.value = this.nbErrors + " typo(s)";
			
			// check done to avoid having irrelavant stats
			gDialog.wpmRecent.value = averageShifted(this.wpmArr, 1, wpm) + " wpm";
			gDialog.cpmRecent.value = averageShifted(this.cpmArr, 0, cpm) + " cpm";
			gDialog.errRecent.value = averageShifted(this.errArr, 1, err) + " %";
		}
	
		// Allow next level?
		var allow = false;
		if (this.nbTries >= nbRequiredSuccesses)
			allow = ((average(this.cpmArr, 0) > minSpeed) && (average(this.errArr, 1) < maxTypos));
		return allow;
	},
	reset : function() {
		this.nbTries = 0;
		this.wpmSum = 0;
		this.cpmSum = 0;
		this.errSum = 0;
		this.wpmArr = new Array();
		this.cpmArr = new Array();
		this.errArr = new Array();
	}
};

function average(arr, nbDecimals) {
	var sum = 0;
	for ( var i = 0; i < arr.length; i++)
		sum += arr[i];
	var mult = Math.pow(10, nbDecimals);
	return Math.round(mult * sum / arr.length) / mult;
}

function averageShifted(arr, nbDecimals, additionalValue) {
	var sum = additionalValue;
	var i;
	var length;
	if (arr.length > nbRequiredSuccesses) {
		i = 1;
		length = arr.length;
	} else {
		i = 0;
		length = arr.length+1;
	}
	for (; i < arr.length; i++)
		sum += arr[i];
	var mult = Math.pow(10, nbDecimals);
	return Math.round(mult * sum / length) / mult;
}

function Startup() {
	gDialog.txtPrompt = document.getElementById("txtPrompt");
	gDialog.txtAnswer = document.getElementById("txtAnswer");
	gDialog.lShiftKey = document.getElementById('LFSH');
	gDialog.rShiftKey = document.getElementById('RTSH');
	gDialog.bspaceKey = document.getElementById('BKSP');
	gDialog.keyboard = document.getElementById('keyboard');
	gDialog.layout = document.getElementById('layout');
	gDialog.variant = document.getElementById('variant');
	gDialog.level = document.getElementById('level');
	// timer
	gDialog.wpmRecent = document.getElementById("wpm-recent");
	gDialog.cpmRecent = document.getElementById("cpm-recent");
	gDialog.errRecent = document.getElementById("err-recent");
	gDialog.wpmCurrent = document.getElementById("wpm-current");
	gDialog.cpmCurrent = document.getElementById("cpm-current");
	gDialog.errCurrent = document.getElementById("err-current");
	gDialog.wpmAverage = document.getElementById("wpm-average");
	gDialog.cpmAverage = document.getElementById("cpm-average");
	gDialog.errAverage = document.getElementById("err-average");

	switchKeyboardStyle(defaultKeyboardStyle);
	gDialog.txtPrompt.clickSelectsAll = true;

	loadLayouts(defaultKeyboardLayout, defaultKeyboardLayoutVariant);
	loadLessons(defaultLesson);
	gDialog.mode = defaultMode;
	gTimer.reset();
}

function newPrompt(value) {
	// display a new exercise and start the test
	highlightKey(value.charAt(0));
	gDialog.txtPrompt.value = value;
	gDialog.txtAnswer.value = "";
	gDialog.txtAnswer.focus();
}

function newPromptFromLessons() {
	// select a random line in the current level
	var level = gDialog.level.selectedIndex;
	var lines = gLessons[level].getElementsByTagName("Line");
	var i = Math.floor(Math.random() * lines.length);
	newPrompt(lines[i].childNodes[0].nodeValue);
}

function newPromptFromPangrams() {
	// select a random pangram
	var i = Math.floor(Math.random() * gPangrams.length);
	newPrompt(gPangrams[i]);
}

function newPromptFromClipboard() {
	// see: http://www.xulplanet.com/tutorials/mozsdk/clipboard.php

	// XPConnect privilege needed
	netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');

	// Create an interface to the clipboard
	var clip = Components.classes["@mozilla.org/widget/clipboard;1"]
			.createInstance(Components.interfaces.nsIClipboard);
	if (!clip)
		return false;

	// Create a transferable interface
	var trans = Components.classes["@mozilla.org/widget/transferable;1"]
			.createInstance(Components.interfaces.nsITransferable);
	if (!trans)
		return false;

	// specify the transferable data type (text)
	trans.addDataFlavor("text/unicode");

	// get clipboard data
	var pasteText = "";
	try {
		var str = new Object();
		var strLength = new Object();
		clip.getData(trans, clip.kGlobalClipboard);
		trans.getTransferData("text/unicode", str, strLength);
		if (str)
			str = str.value
					.QueryInterface(Components.interfaces.nsISupportsString);
		if (str)
			pasteText = str.data.substring(0, strLength.value / 2);
	} catch (e) {
		// empty clipboard or unknown error, aborting
		return;
	}
	;

	// paste clipboard data into the prompt box
	if (!pasteText.length)
		return;
	else if (pasteText.length > 100) {
		// truncate to the first 100 characters
		pasteText = pasteText.substring(0, 100);
		pasteText = pasteText.substring(0, pasteText.lastIndexOf(" ") - 1);
	}
	newPrompt(pasteText);
}

function nextPrompt() {
	gTimer.stop();
	var nextLevel = gTimer.report(true);

	if (gDialog.mode == modLESSONS) {
		if (nextLevel) { // get to the next level
			alert("Congrats!\nLet's get to the next level.");
			gTimer.reset();
			gDialog.level.selectedIndex++;
		}
		newPromptFromLessons();
	} else if (gDialog.mode == modPANGRAMS) {
		newPromptFromPangrams();
	} else if (gDialog.mode == modCLIPBOARD) {
	}
}

function textInput(value) {
	if (!value.length) {
		// empty input box => reset timer + current stats
		gDialog.wpmCurrent.value = "";
		gDialog.cpmCurrent.value = "";
		gDialog.errCurrent.value = "";
		gTimer.stop();
		highlightKey(gDialog.txtPrompt.value.charAt(0));
		gDialog.txtPrompt.setSelectionRange(0, 0);
		return;
	}

	var pos = value.length - 1;
	if (pos == 0) { // first char => start the timer
		gTimer.start();
	}

	// Check the last char is correct
	var entered = value.substring(pos);
	var expected = gDialog.txtPrompt.value.charAt(pos);
	if (entered != expected) {
		// mistake
		gTimer.typo();
	}

	// Check the whole input is correct
	var correct = (value == gDialog.txtPrompt.value.substr(0, pos + 1));
	gDialog.txtAnswer.setAttribute("class", correct ? "" : "error");
	if (correct) {
		// highlight the next key (or remove highlighting if it's finished)
		highlightKey(gDialog.txtPrompt.value.charAt(pos + 1));
		// finished?
		if (pos >= gDialog.txtPrompt.value.length - 1) {
			nextPrompt();
		} else {
			gDialog.txtPrompt.setSelectionRange(pos + 1, pos + 2);
		}
	} else {
		// highlight the backspace key
		highlightSpecialKey(gDialog.bspaceKey);
	}
	gTimer.report(false);
}

function highlightKey(key) {
	// remove last key's highlighting
	if (gDialog.lastKey)
		gDialog.lastKey.removeAttribute("style");
	if (gDialog.lastMod)
		gDialog.lastMod.removeAttribute("style");

	// return if no new key
	if (!key || !key.length)
		return;

	// highlight the new key
	// Note: an opacity of 100% hides the right-shift key with the typematrix
	// layout... :-/
	var style = "opacity: 0.99; border-style: inset;";
	gDialog.currKey = key;
	gDialog.lastKey = gKeymap[key];
	if (gDialog.lastKey)
		gDialog.lastKey.setAttribute("style", style);
	gDialog.lastMod = gKeymod[key];
	if (gDialog.lastMod)
		gDialog.lastMod.setAttribute("style", style);
}

function highlightSpecialKey(keyID) {
	// remove last key's highlighting
	if (gDialog.lastKey)
		gDialog.lastKey.removeAttribute("style");
	if (gDialog.lastMod)
		gDialog.lastMod.removeAttribute("style");

	// return if no new key
	if (!keyID)
		return;

	// highlight the new key
	gDialog.lastKey = keyID;
	gDialog.lastKey.setAttribute("style", "opacity: 1");
	gDialog.lastMod = null;
}

function switchKeyboardStyle(style) {
	if (style == "standard") {
		// style = style.substring(0, style.length-1);
		document.getElementById("AE01").setAttribute("class", "left5");
		document.getElementById("AE02").setAttribute("class", "left5");
		document.getElementById("AE03").setAttribute("class", "left4");
		document.getElementById("AE04").setAttribute("class", "left3");
		document.getElementById("AE05").setAttribute("class", "left2");
		document.getElementById("AE06").setAttribute("class", "left2");
		document.getElementById("AE07").setAttribute("class", "right2");
		document.getElementById("AE08").setAttribute("class", "right2");
		document.getElementById("AE09").setAttribute("class", "right3");
		document.getElementById("AE10").setAttribute("class", "right4");
	} else if (style == "us") {
		// style = style.substring(0, style.length-1);
		document.getElementById("AE01").setAttribute("class", "left5");
		document.getElementById("AE02").setAttribute("class", "left4");
		document.getElementById("AE03").setAttribute("class", "left3");
		document.getElementById("AE04").setAttribute("class", "left2");
		document.getElementById("AE05").setAttribute("class", "left2");
		document.getElementById("AE06").setAttribute("class", "left2");
		document.getElementById("AE07").setAttribute("class", "right2");
		document.getElementById("AE08").setAttribute("class", "right3");
		document.getElementById("AE09").setAttribute("class", "right4");
		document.getElementById("AE10").setAttribute("class", "right5");
	} else if (style == "typematrix") {
		document.getElementById("AE01").setAttribute("class", "left5");
		document.getElementById("AE02").setAttribute("class", "left4");
		document.getElementById("AE03").setAttribute("class", "left3");
		document.getElementById("AE04").setAttribute("class", "left2");
		document.getElementById("AE05").setAttribute("class", "left2");
		document.getElementById("AE06").setAttribute("class", "right2");
		document.getElementById("AE07").setAttribute("class", "right2");
		document.getElementById("AE08").setAttribute("class", "right3");
		document.getElementById("AE09").setAttribute("class", "right4");
		document.getElementById("AE10").setAttribute("class", "right5");
	} else
		alert("Unhandled style " + style);
	gDialog.keyboard.setAttribute("class", style);
	gDialog.txtAnswer.focus();
}

function toggleKeyboardVisibility(myself) {
	myself.label = gDialog.keyboard.hidden ? "Hide keyboard" : "Show keyboard";
	gDialog.keyboard.hidden = !gDialog.keyboard.hidden;
	gDialog.txtAnswer.focus();
}
